/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef TIMELINE_H
#define TIMELINE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QVariantList>
#include <QMap>
#include <QPushButton>
#include <QtCore/qmath.h>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "asobject.h"
#include "asactivity.h"
#include "post.h"
#include "filterchecker.h"
#include "pageselector.h"


class TimeLine : public QWidget
{
    Q_OBJECT

public:
    TimeLine(PumpController::requestTypes timelineType,
             PumpController *pumpController,
             GlobalObject *globalObject,
             FilterChecker *filterChecker,
             QWidget *parent = 0);
    ~TimeLine();

    void createKeyboardActions();

    void setCustomUrl(QString url);

    void clearTimeLineContents(bool showMessage=true);
    void removeOldPosts(int minimumToKeep);
    void insertSeparator(int position);

    int getCurrentPage();
    int getTotalPages();
    int getTotalPosts();
    void updateCurrentPageNumber();
    void syncPostsPerPage();
    void enablePaginationButtons();
    void disablePaginationButtons();

    void resizePosts(QList<Post *> postsToResize, bool resizeAll=false);

    void markPostsAsRead();

    void updateFuzzyTimestamps();

    bool commentingOnAnyPost();
    void notifyBlockedUpdates();

    QList<Post *> getPostsInTimeline();

    QFrame *getSeparatorWidget();

    void showMessage(QString message);


signals:
    void scrollTo(QAbstractSlider::SliderAction sliderAction);

    void timelineRendered(PumpController::requestTypes timelineType,
                          int newPostCount,
                          int highlightedPostsCount,
                          int directPostsCount,
                          int highlightedByFilterCount,
                          int deletedPostsCount,
                          int filteredPostsCount,
                          int pendingForNextUpdate,
                          QString currentPageAndTotal);
    void unreadPostsCountChanged(PumpController::requestTypes timelineType,
                                 int newPostCount,
                                 int highlightedCount,
                                 int fullTimelinePostCount);

    void commentingOnPost(QWidget *commenterWidget);


public slots:
    void setTimeLineContents(QVariantList postList, QString previousLink,
                             QString nextLink, int totalItems);
    void onUpdateFailed(int requestType);

    void updatePostsFromMinorFeed(ASObject *object);
    void addLikesFromMinorFeed(QString objectId, QString objectType,
                               QString actorId, QString actorName,
                               QString actorUrl);
    void removeLikesFromMinorFeed(QString objectId, QString objectType,
                                  QString actorId);
    void addReplyFromMinorFeed(ASObject *object);
    void setPostsDeletedFromMinorFeed(ASObject *object);


    void setLikesInPost(QVariantList likesList, QString originatingPostUrl);
    void setCommentsInPost(QVariantList commentsList, QString originatingPostUrl);
    //void setSharesInPost(...)

    void goToFirstPage();
    void goToPreviousPage();
    void goToNextPage();
    void goToSpecificPage(int pageNumber);
    void getNewPending();

    void showPageSelector();

    void scrollUp();
    void scrollDown();
    void scrollPageUp();
    void scrollPageDown();
    void scrollToTop();
    void scrollToBottom();

    void decreaseUnreadPostsCount(bool wasHighlighted);

    void updateAvatarFollowStates();


protected:


private:
    QVBoxLayout *m_mainLayout;
    QVBoxLayout *m_postsLayout;
    QHBoxLayout *m_bottomLayout;

    QPushButton *m_getNewPendingButton;
    QLabel *m_infoLabel;

    QWidget *m_postsWidget;
    QFrame *m_separatorFrame;

    QPushButton *m_firstPageButton;
    QPushButton *m_previousPageButton;
    QPushButton *m_currentPageButton;
    QPushButton *m_nextPageButton;


    QString m_customUrl;
    QString m_previousPageLink;
    QString m_nextPageLink;
    int m_fullTimelinePostCount;
    int m_postsPendingForNextTime;
    bool m_firstLoad;
    bool m_gettingNew;
    bool m_wasOnFirstPage;

    int m_timelineOffset;
    int m_oldTimelineOffset;
    int m_postsPerPage;
    int m_unreadPostsCount;
    int m_highlightedPostsCount;
    QString m_currentPageTotalString;


    // QActions to enhance keyboard control
    QAction *m_scrollUpAction;
    QAction *m_scrollDownAction;
    QAction *m_scrollPageUpAction;
    QAction *m_scrollPageDownAction;
    QAction *m_scrollTopAction;
    QAction *m_scrollBottomAction;
    QAction *m_previousPageAction;
    QAction *m_nextPageAction;
    QAction *m_showPageSelectorAction;


    PageSelector *m_pageSelector;


    PumpController::requestTypes m_timelineType;
    bool m_isFavoritesTimeline;

    QString m_previousNewestPostId;

    QList<Post *> m_postsInTimeline;
    QStringList m_objectsIdList;


    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
    FilterChecker *m_filterChecker;
};

#endif // TIMELINE_H
