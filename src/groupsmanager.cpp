/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "groupsmanager.h"

GroupsManager::GroupsManager(PumpController *pumpController,
                             QWidget *parent) : QWidget(parent)
{
    m_pumpController = pumpController;

    this->setWindowTitle("GROUPS MANAGER"  " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("user-group-properties"));
    this->setWindowFlags(Qt::Dialog);
    this->setWindowModality(Qt::ApplicationModal);
    this->setMinimumSize(480, 460);

    m_newGroupNameLineEdit = new QLineEdit(this);
    m_newGroupNameLineEdit->setPlaceholderText("Name for the new group");

    m_newGroupSummaryLineEdit = new QLineEdit(this);
    m_newGroupSummaryLineEdit->setPlaceholderText("Summary");

    m_newGroupDescLineEdit = new QLineEdit(this);
    m_newGroupDescLineEdit->setPlaceholderText("Longer description of the group");

    m_createGroupButton = new QPushButton(QIcon::fromTheme("user-group-new"),
                                          "CREATE GROUP",
                                          this);
    connect(m_createGroupButton, &QAbstractButton::clicked,
            this, &GroupsManager::createGroup);


    m_joinLeaveGroupIdLineEdit = new QLineEdit(this);
    m_joinLeaveGroupIdLineEdit->setPlaceholderText("ID of the group you wish "
                                                   "to join or leave");

    m_joinGroupButton = new QPushButton(QIcon::fromTheme("list-add-user"),
                                        "JOIN!",
                                        this);
    connect(m_joinGroupButton, &QAbstractButton::clicked,
            this, &GroupsManager::joinGroup);

    m_leaveGroupButton = new QPushButton(QIcon::fromTheme("list-remove-user"),
                                         "LEAVE",
                                         this);
    connect(m_leaveGroupButton, &QAbstractButton::clicked,
            this, &GroupsManager::leaveGroup);



    m_closeAction = new QAction(this);
    m_closeAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(m_closeAction, &QAction::triggered,
            this, &QWidget::hide);
    this->addAction(m_closeAction);

    // Layout
    m_layout = new QVBoxLayout();
    m_layout->setAlignment(Qt::AlignTop);
    m_layout->addWidget(m_newGroupNameLineEdit);
    m_layout->addWidget(m_newGroupSummaryLineEdit);
    m_layout->addWidget(m_newGroupDescLineEdit);
    m_layout->addWidget(m_createGroupButton);
    m_layout->addStretch(1);
    m_layout->addWidget(m_joinLeaveGroupIdLineEdit);
    m_layout->addWidget(m_joinGroupButton);
    m_layout->addWidget(m_leaveGroupButton);
    this->setLayout(m_layout);

    qDebug() << "GroupsManager created";

}

GroupsManager::~GroupsManager()
{
    qDebug() << "GroupsManager destroyed";
}


////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SLOTS /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void GroupsManager::createGroup()
{
    if (m_newGroupNameLineEdit->text().trimmed().isEmpty())
    {
        QMessageBox::warning(this, "ERROR",
                             "Groups require a name.");
        return;
    }
    qDebug() << "Creating group...";

    m_pumpController->createGroup(m_newGroupNameLineEdit->text().trimmed(),
                                  m_newGroupSummaryLineEdit->text().trimmed(),
                                  m_newGroupDescLineEdit->text().trimmed());

    m_newGroupNameLineEdit->clear();
    m_newGroupSummaryLineEdit->clear();
    m_newGroupDescLineEdit->clear();
}


void GroupsManager::deleteGroup()
{

}


void GroupsManager::joinGroup()
{
    if (m_joinLeaveGroupIdLineEdit->text().trimmed().isEmpty())
    {
        QMessageBox::warning(this, "ERROR",
                             "ID of group to join is empty.");
        return;
    }

    qDebug() << "Joining group...";
    m_pumpController->joinGroup(m_joinLeaveGroupIdLineEdit->text().trimmed());

    m_joinLeaveGroupIdLineEdit->clear();
}


void GroupsManager::leaveGroup()
{
    if (m_joinLeaveGroupIdLineEdit->text().trimmed().isEmpty())
    {
        QMessageBox::warning(this, "ERROR",
                             "ID of group to leave is empty.");
        return;
    }
    qDebug() << "Leaving group...";
    m_pumpController->leaveGroup(m_joinLeaveGroupIdLineEdit->text().trimmed());

    m_joinLeaveGroupIdLineEdit->clear();
}
