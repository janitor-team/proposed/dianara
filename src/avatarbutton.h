/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef AVATARBUTTON_H
#define AVATARBUTTON_H

#include <QToolButton>
#include <QMenu>
#include <QMessageBox>
#include <QScrollArea>

#include <QDebug>

#include "asperson.h"
#include "pumpcontroller.h"
#include "globalobject.h"
#include "mischelpers.h"


class AvatarButton : public QToolButton
{
    Q_OBJECT

public:
    explicit AvatarButton(ASPerson *person,
                          PumpController *pumpController,
                          GlobalObject *globalObject,
                          QSize avatarSize,
                          QWidget *parent = 0);
    ~AvatarButton();

    void setGenericAvatarIcon();
    void updateAvatarIcon(QString filename);

    void createAvatarMenu();
    void syncFollowState(bool firstTime=false);
    void setFollowUnfollow();

    void addSeparatorToMenu();
    void addActionToMenu(QAction *action);


signals:


public slots:
    void openAuthorProfileInBrowser();
    void followUser();
    void unfollowUser();
    void sendMessageToUser();
    void browseUserMessages();

    void redrawAvatar(QString avatarUrl, QString avatarFilename);


private:
    QMenu *m_avatarMenu;
    QAction *m_avatarMenuIdAction;
    QAction *m_avatarMenuProfileAction;
    QAction *m_avatarMenuFollowAction;
    QAction *m_avatarMenuMessageAction;
    QAction *m_avatarMenuBrowseAction;

    int m_iconWidth;

    QString m_authorId;
    QString m_authorName;
    QString m_authorUrl;
    QString m_authorAvatarUrl;
    QString m_authorOutbox;
    bool m_authorFollowed;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // AVATARBUTTON_H
