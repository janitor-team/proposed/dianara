/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONTACTMANAGER_H
#define CONTACTMANAGER_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QVariantList>
#include <QMap>
#include <QString>
#include <QPushButton>
#include <QLineEdit>
#include <QTabWidget>
#include <QScrollArea>
#include <QMenu>
#include <QFileDialog>
#include <QResizeEvent>

#include <QDebug>

#include "pumpcontroller.h"
#include "contactlist.h"
#include "listsmanager.h"
#include "globalobject.h"
#include "siteuserslist.h"



class ContactManager : public QWidget
{
    Q_OBJECT

public:
    ContactManager(PumpController *pumpController,
                   GlobalObject *globalObject,
                   QWidget *parent = 0);
    ~ContactManager();

    void setTabLabels();

    void exportContactsToFile(QString listType);

    void enableManualFollowWidgets();


signals:


public slots:
    void setContactListsContents(QString listType,
                                 QVariantList contactList,
                                 int totalReceivedCount);
    void setListsListContents(QVariantList listsList);

    void changeFollowingCount(int difference);

    void refreshFollowing();
    void refreshFollowers();

    void exportFollowing();
    void exportFollowers();

    void refreshPersonLists();

    void toggleFollowButton(QString currentAddress);

    void validateContactId();
    void followContact(QString userId, int httpCode,
                       bool requestTimedOut, QString serverVersion);
    void onCantFollowNow(QString userId);


protected:


private:
    QVBoxLayout *m_mainLayout;

    QHBoxLayout *m_topLayout;
    QLabel *m_enterAddressLabel;
    QLineEdit *m_addressLineEdit;
    QPushButton *m_followButton;

    QTabWidget *m_tabWidget;

    ContactList *m_followingWidget;
    int m_followingCount;

    ContactList *m_followersWidget;
    int m_followersCount;

    ListsManager *m_listsManager;
    QScrollArea *m_listsScrollArea;
    int m_listsCount;

    SiteUsersList *m_siteUsersList;

    QPushButton *m_optionsButton;
    QMenu *m_optionsMenu;

    bool m_followingManually;
    QString m_followingAddress;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // CONTACTMANAGER_H
