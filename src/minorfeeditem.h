/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MINORFEEDITEM_H
#define MINORFEEDITEM_H

#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QFile>
#include <QPushButton>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "timestamp.h"
#include "asactivity.h"
#include "post.h"
#include "avatarbutton.h"
#include "filterchecker.h"
#include "filtermatcheswidget.h"



class MinorFeedItem : public QFrame
{
    Q_OBJECT

public:
    explicit MinorFeedItem(ASActivity *activity,
                           bool highlightedByFilter,
                           PumpController *pumpController,
                           GlobalObject *globalObject,
                           QWidget *parent = 0);
    ~MinorFeedItem();

    void setItemAsNew(bool isNew, bool informFeed);
    bool isNew();
    void setFuzzyTimeStamp();
    void syncAvatarFollowState();

    int getItemHighlightType();
    QString getActivityId();

    
signals:
    void itemRead(bool wasHighlighted);

    
public slots:
    void openOriginalPost();
    void showUrlInfo(QString url);


protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void leaveEvent(QEvent *event);
    virtual void paintEvent(QPaintEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_topLayout;
    QVBoxLayout *m_leftLayout;
    QVBoxLayout *m_rightLayout;
    QHBoxLayout *m_rightLowerLayout;


    int m_verbIconType;
    QStringList m_verbIcons;
    int m_verbIconSize;
    int m_verbIconSizeSubtle;

    AvatarButton *m_avatarButton;
    QLabel *m_timestampLabel;
    QLabel *m_activityDescriptionLabel;

    QPushButton *m_openButton;

    QVariantMap m_originalObjectMap;
    QVariantMap m_inReplyToMap;

    AvatarButton *m_targetAvatarButton;
    bool m_haveTargetAvatar;

    FilterMatchesWidget *m_filterMatchesWidget;

    bool m_itemIsOwn;
    bool m_itemIsNew;
    int m_itemHighlightType;
    QString m_createdAtTimestamp;
    QString m_activityId;


    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // MINORFEEDITEM_H
