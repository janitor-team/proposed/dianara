/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include <QObject>
#include <QDateTime>

#include <QDebug>


class Timestamp : public QObject
{
    Q_OBJECT

public:
    explicit Timestamp(QObject *parent = 0);

    // isoTime = ISO 8601, UTC, like "2012-02-07T01:32:02Z"
    static QString localTimeDate(QString isoTime); // returns "07-02-2012\n01:32:02"
    static QString fuzzyTime(QString isoTime);     // returns "about an hour ago", etc.

signals:

public slots:

};

#endif // TIMESTAMP_H
